basedir=$(realpath $(dirname $0))
echo "==================================================================="
echo "Initializing ... "
echo "$basedir"
rm -rf "$basedir/bin"
rm -rf "$basedir/log"
rm -rf "$basedir/output"
mkdir -p "$basedir/bin"
mkdir -p "$basedir/log"

echo "==================================================================="
echo "1) Heat 2D explict"

echo "    Compiling ... "
ifort "$basedir/src/head2D_explict.f90" -o "$basedir/bin/head2D_explicit"

echo "    Executing ... "
"$basedir/bin/head2D_explicit" $2 &>  "$basedir/log/head2D_explicit_dat.log"

echo "    Ploting ... "
"$basedir/scripts/dat2png.py" --dir "$basedir" --pattern "Heat2D_explicit" --size $2  --type "2D" &>  "$basedir/log/head2D_explicit_plot.log"

echo "    Moving output ... "
mkdir -p "$basedir/output/dat/"
mkdir -p "$basedir/output/png/"
find -maxdepth 1 -type f -name '*.dat' -exec mv {} "$basedir/output/dat/" \;
find -maxdepth 1 -type f -name '*.png' -exec mv {} "$basedir/output/png/" \;

echo "==================================================================="
echo "2) Heat 2D exact solution"

echo "    Compiling ... "
ifort "$basedir/src/head2D_exact.f90" -o "$basedir/bin/head2D_exact"

echo "    Executing ... "
"$basedir/bin/head2D_exact" $2 &>  "$basedir/log/head2D_exact_dat.log"
cat $(find -maxdepth 1 -type f -name '*.dat' | sort | tail -1 ) | head -n $2 > Heat_exact.csv

echo "    Ploting ... "
"$basedir/scripts/dat2png.py" --dir "$basedir" --pattern "Heat2D_exact"  --size $2  --type "2D" &>  "$basedir/log/head2D_exact_plot.log"

echo "    Moving output ... "
mkdir -p "$basedir/output/dat/"
mkdir -p "$basedir/output/png/"
mkdir -p "$basedir/output/csv/"

find -maxdepth 1 -type f -name '*.dat' -exec mv {} "$basedir/output/dat/" \;
find -maxdepth 1 -type f -name '*.png' -exec mv {} "$basedir/output/png/" \;
find -maxdepth 1 -type f -name '*.csv' -exec mv {} "$basedir/output/csv/" \;


echo "==================================================================="

echo "3) Heat 2D explicit with MPI directive (Computation Time Test)"

echo "    Compiling ... "
mpiifort "$basedir/src/head2D_explict_mpi.f90" -o "$basedir/bin/head2D_explict_mpi"
#mpiifort "$basedir/src/head2D_explict_mpi.f90"  -L$MKLPATH -I$MKLINCLUDE $MKLPATH/libmkl_intel_lp64.a $MKLPATH/libmkl_intel_thread.a $MKLPATH/libmkl_core.a $MKLPATH/libmkl_intel_lp64.a $MKLPATH/libmkl_intel_thread.a $MKLPATH/libmkl_core.a -liomp5 -lpthread -lm -o "$basedir/bin/head2D_explict_mpi"

echo "    Executing ... "
echo "  -----------------------------------------------------------------"
echo "  | nCPU: 1 "
/usr/bin/time -f "1 %E" -o "performance.csv" -a mpiexec -n 1 "$basedir/bin/head2D_explict_mpi" $2 >>  "$basedir/log/head2D_explict_mpi_dat_1.log" 
echo "  -----------------------------------------------------------------"
echo "  | nCPU: 2 "
/usr/bin/time -f "2 %E" -o "performance.csv"  -a mpiexec -n 2 "$basedir/bin/head2D_explict_mpi" $2 >>  "$basedir/log/head2D_explict_mpi_dat_2.log" 
echo "  -----------------------------------------------------------------"
echo "  | nCPU: 4 "
/usr/bin/time -f "4 %E" -o "performance.csv"  -a mpiexec -n 4 "$basedir/bin/head2D_explict_mpi" $2 >>  "$basedir/log/head2D_explict_mpi_dat_4.log" 
echo "  -----------------------------------------------------------------"
echo "  | nCPU: 6 "
/usr/bin/time -f "6 %E" -o "performance.csv"  -a mpiexec -n 6 "$basedir/bin/head2D_explict_mpi" $2 >>  "$basedir/log/head2D_explict_mpi_dat_6.log" 
echo "  -----------------------------------------------------------------"
echo "  | nCPU: 8 "
/usr/bin/time -f "8 %E" -o "performance.csv" -a mpiexec -n 8 "$basedir/bin/head2D_explict_mpi" $2 >>  "$basedir/log/head2D_explict_mpi_dat_8.log"
echo "  -----------------------------------------------------------------"


echo "    Ploting ... "
"$basedir/scripts/dat2png.py" --dir "$basedir" --pattern "Heat2D_mpi"  --size $2 --type "2D" &>  "$basedir/log/head2D_mpi_plot.log"
"$basedir/scripts/dat2png.py" --dir "$basedir" --pattern "performance"  --size $2 --type "P" &>  "$basedir/log/head2D_mpi_performance_plot.log"


echo "    Moving output ... "
mkdir -p "$basedir/output/dat/"
mkdir -p "$basedir/output/png/"
find -maxdepth 1 -type f -name '*.dat' -exec mv {} "$basedir/output/dat/" \;
find -maxdepth 1 -type f -name '*.png' -exec mv {} "$basedir/output/png/" \;


echo "==================================================================="

echo "4) Heat 2D explicit with MPI directive (Heat distribution)"

echo "    Compiling ... "
mpiifort "$basedir/src/head2D_explict_mpi.f90" -o "$basedir/bin/head2D_explict_mpi"

echo "    Executing ... "
echo "  -----------------------------------------------------------------"
/usr/bin/time -f "  | GridSize: 40 , Time: %E" -o "/dev/stderr" mpiexec -n $1 "$basedir/bin/head2D_explict_mpi" 4 >>  "$basedir/log/head2D_explict_mpi_dat_04.log"
cat $(find -maxdepth 1 -type f -name '*.dat' | sort | tail -1 ) | head -4 > Heat1D_004.csv
find -maxdepth 1 -type f -name '*.dat' -exec rm {}  \;

echo "  -----------------------------------------------------------------"
/usr/bin/time -f "  | GridSize: 80 , Time: %E" -o "/dev/stderr" mpiexec -n $1 "$basedir/bin/head2D_explict_mpi" 8 >>  "$basedir/log/head2D_explict_mpi_dat_08.log"
cat $(find -maxdepth 1 -type f -name '*.dat' | sort | tail -1 ) | head -8 > Heat1D_008.csv
find -maxdepth 1 -type f -name '*.dat' -exec rm {}  \;

echo "  -----------------------------------------------------------------"
/usr/bin/time -f "  | GridSize: 160 , Time: %E" -o "/dev/stderr" mpiexec -n $1 "$basedir/bin/head2D_explict_mpi" 12 >>  "$basedir/log/head2D_explict_mpi_dat_016.log"
cat $(find -maxdepth 1 -type f -name '*.dat' | sort | tail -1 ) | head -16 > Heat1D_016.csv
find -maxdepth 1 -type f -name '*.dat' -exec rm {}  \;

echo "  -----------------------------------------------------------------"
/usr/bin/time -f "  | GridSize: 240 , Time: %E" -o "/dev/stderr" mpiexec -n $1 "$basedir/bin/head2D_explict_mpi" 16 >>  "$basedir/log/head2D_explict_mpi_dat_024.log"
cat $(find -maxdepth 1 -type f -name '*.dat' | sort | tail -1 ) | head -24 > Heat1D_024.csv
find -maxdepth 1 -type f -name '*.dat' -exec rm {}  \;

echo "  -----------------------------------------------------------------"
/usr/bin/time -f "  | GridSize: 400 , Time: %E" -o "/dev/stderr" mpiexec -n $1 "$basedir/bin/head2D_explict_mpi" 20 >>  "$basedir/log/head2D_explict_mpi_dat_040.log"
cat $(find -maxdepth 1 -type f -name '*.dat' | sort | tail -1 ) | head -40 > Heat1D_040.csv
find -maxdepth 1 -type f -name '*.dat' -exec rm {}  \;

echo "    Ploting ... "
"$basedir/scripts/dat2png.py" --dir "$basedir" --pattern "Heat1D"  --size $2 --type "1D" &>  "$basedir/log/head2D_mpi_plot_section.log"

echo "    Moving output ... "
mkdir -p "$basedir/output/csv/"
find -maxdepth 1 -type f -name '*.csv' -exec mv {} "$basedir/output/csv/" \;
find -maxdepth 1 -type f -name '*.png' -exec mv {} "$basedir/output/png/" \;


echo "Done! ..."
