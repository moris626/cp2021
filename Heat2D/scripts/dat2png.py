#!/usr/bin/python3 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.transforms as t
import numpy as np
from numpy import genfromtxt
import os
import argparse
from datetime import datetime


def heat2D(search_dir, pattern, size):
    for root, dirs, files in os.walk(search_dir):
        for filename in files:
            if root != search_dir:
                continue
            if filename.startswith(pattern):
                my_data = genfromtxt(os.path.join(root,filename))
                data=my_data[:,2].reshape((size,size))
                plt.imshow(data, interpolation='nearest', cmap=cm.RdBu ,extent=(0,1,0,1),vmin=50,vmax=100)
                plt.title('Heat distribution : %s' %pattern)
                plt.xlabel('x axis')
                plt.ylabel('y axis')
                plt.savefig(filename.replace('dat','png'))

def heatSection(search_dir, pattern, size):
    my_data_exact = genfromtxt(os.path.join(search_dir,'output/csv/Heat_exact.csv'))
    data_exact = my_data_exact[:,2]
    x_exact = my_data_exact[:,0]
    for root, dirs, files in os.walk(search_dir):
        for filename in files:
            if root != search_dir:
                continue
            if filename.startswith(pattern):
                print(filename)
                my_data = genfromtxt(os.path.join(root,filename))
                data=my_data[:,2]
                x=my_data[:,0]
                plt.clf()
                plt.plot(x, data ,'.',clip_box = t.Bbox.from_extents(0, 1, 0, 1),label="grid = {}x{}".format(len(data),len(data)))
                plt.plot(x_exact, data_exact,clip_box = t.Bbox.from_extents(0, 1, 0, 1),label="exact")
                plt.legend(loc='upper right', frameon=False)
                plt.title('Heat section ')
                plt.xlabel('x axis')
                plt.ylabel('temperature')
                plt.savefig(filename.replace('.csv','_section.png'))

def performance(search_dir, pattern, size):
    for root, dirs, files in os.walk(search_dir):
        for filename in files:
            if root != search_dir:
                continue
            if filename.startswith(pattern):
                time = []
                nproc = []
                fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
                with open(os.path.join(root,filename),'r') as f:
                    for line in f:
                       time.append(float(datetime.strptime(line.split()[1], "%M:%S.%f").strftime('%S.%f')))
                       nproc.append(int(line.split()[0]))
                speedup =  time[0] / np.array(time)
                ax1.plot(nproc, speedup.tolist())
                ax1.set_title('Speedup')
                ax1.set_xlabel('p')
                ax1.set_ylabel('Speedup')
                efficency = speedup / np.array(nproc)
                ax2.plot(nproc, efficency.tolist())
                ax2.set_title('Efficency')
                ax2.set_xlabel('p')
                ax2.set_ylabel('Efficency')   
                kuck=speedup *efficency
                ax3.plot(nproc, kuck.tolist())
                ax3.set_title('Kuck')
                ax3.set_xlabel('p')
                ax3.set_ylabel('Kuck')    
                fig.suptitle('Parallel Processing Analysis', fontsize=16)            
                fig.savefig('performance.png')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='dat2png')
    parser.add_argument('--dir',dest='dir',metavar='DIR',required=True)
    parser.add_argument('--pattern',dest='pattern',metavar='PATTERN',required=True)
    parser.add_argument('--size',dest='size',metavar='SIZE',required=True,type=int)
    parser.add_argument('--type',dest='type',metavar='TYPE',required=True)
    args = parser.parse_args()
    if args.type == '1D':
        heatSection(args.dir,args.pattern,args.size)
    elif args.type == '2D':
        heat2D(args.dir,args.pattern,args.size)
    else:
        performance(args.dir,args.pattern,args.size)
