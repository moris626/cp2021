PROGRAM Heat2D
    !------------------------------------------------------------!
    IMPLICIT NONE
    !------------------------------------------------------------!
    INTEGER             :: i, j, n         ! space and time index
    !
    CHARACTER(10)       :: cellchar
    INTEGER             :: IMAX            ! total number of cells 
    REAL                :: xL, xR          ! left and right domain definition
    REAL                :: yU, yD          ! up and down domain definition
    REAL                :: xD              ! location of discontinuity
    REAL                :: dx, dx2         ! mesh size and square of mesh size
    REAL, ALLOCATABLE   :: x(:), y(:)      ! vertex coords
    !
    REAL                :: CFL             ! Courant-Friedrichs-Lewy number for stability condition (CFL<1) 
    REAL                :: time            ! current time
    REAL                :: dt              ! time step
    REAL                :: tend            ! final time
    INTEGER, PARAMETER  :: NMAX = 1e6      ! maximum number of time steps 
    REAL, ALLOCATABLE   :: T(:,:), T1(:,:) ! temperature 
    !
    CHARACTER(LEN=200)  :: TestName        ! name of test problem
    REAL                :: tio             ! output time and time step 
    REAL                :: dtio            ! output time step
    !
    REAL                :: d               ! stability parameter  
    REAL                :: kappa           ! heat conduction coefficient
    REAL                :: TL, TR          ! boundary conditions
    !------------------------------------------------------------!
    !First, make sure the right number of inputs have been provided
    IF(COMMAND_ARGUMENT_COUNT().NE.1)THEN
      WRITE(*,*)'ERROR, TOTAL NUMBER OF CELLS REQUIRED'
      STOP
    ENDIF
    CALL GET_COMMAND_ARGUMENT(1,cellchar)
    READ(cellchar,*)IMAX
    !
    WRITE(*,'(a)') ' | ====================================================== | '
    WRITE(*,'(a)') ' |      Finite difference code for 2D heat equation       | '
    WRITE(*,'(a)') ' | ====================================================== | '
    WRITE(*,'(a)') ' | '
    !
    !======================================== 
    ! SETTINGS OF THE COMPUTATION
    !========================================
    !
    TestName = 'Heat2D_explicit'
    !
    xL     = 0
    xR     = 1
    yU     = 1
    yD     = 0 
    xD     = 0.5  ! location of the discontinuity
    !
    time   = 0.0
    tend   = 0.1    
    dtio   = 2e-2
    tio    = dtio
    CFL    = 0.9   ! for stability condition CFL<1
    d      = 0.45  ! for stability condition d<0.5
    !
    kappa  = 1     ! heat conduction coefficient 
    !
    ! Boundary conditions
    TL = 100
    TR = 50
    !
    !========================================
    !
    ! Allocate variables
    ALLOCATE( x(IMAX)  )
    ALLOCATE( y(IMAX)  )
    ALLOCATE( T(IMAX,IMAX)  )
    ALLOCATE( T1(IMAX,IMAX) )
    !
    !---------------------------------------- 
    !---------------------------------------- 
    !
    ! 1) Computational domain 
    !
    WRITE(*,'(a)') ' | Building computational domain... '
    !    
    dx    = (xR - xL) / REAL(IMAX)  ! step is the same  
    dx2   = dx**2                   ! for x and y coords
    x(1)  = xL
    y(1)  = yD
    !
    DO i = 1, IMAX
      x(i) = xL + 0.5*dx + (i-1)*dx
      y(i) = yD + 0.5*dx + (i-1)*dx
    ENDDO 
    !
    !---------------------------------------- 
    !---------------------------------------- 
    !
    ! 2) Initial condition
    !
    WRITE(*,'(a)') ' | Assigning initial condition... '
    !
    DO j = 1, IMAX                  ! array with rank >1 
      DO i = 1, IMAX                ! are stored by columns
        IF(x(j).LT.xD) THEN
          T(i,j) = TL
        ELSE
          T(i,j) = TR
        ENDIF  
      ENDDO 
    ENDDO 
    !
    !---------------------------------------- 
    !---------------------------------------- 
    !
    ! 3) Computation: main loop in time
    !
    WRITE(*,'(a)') ' | '
    WRITE(*,'(a)') ' | Explicit finite difference solver. '
    WRITE(*,'(a)') ' | '
    !
    DO n = 1, NMAX
      !
      IF(MOD(n,100).EQ.0) THEN
      !  WRITE(*,'(a,i10)') '  Current timestep = ', n 
      ENDIF
      !
      ! 3.1) Compute the time step
      !
      IF(time.GE.tend) THEN
        EXIT    
      ENDIF
      !
      dt = d * dx2 / kappa
      IF((time+dt).GT.tend) THEN
        dt  = tend - time   
        tio = tend
      ENDIF    
      IF((time+dt).GT.tio) THEN
        dt = tio - time
      ENDIF
      !
      ! 3.2) Numerical scheme: FTCS
      !
      DO j = 2, (IMAX-1)
        DO i = 2, (IMAX-1)
          T1(i,j)= T(i,j)  + kappa*dt/(dx2) * ( T(i-1,j) - T(i,j) - T(i,j) + T(i+1,j) ) + kappa*dt/(dx2) * ( T(i,j-1) - T(i,j) - T(i,j) + T(i,j+1) )
        ENDDO
      ENDDO
      ! BCs
      DO j = 2, IMAX-1
        T1(1,j)    = T1(2,j)              ! up  boundary
        T1(IMAX,j) = T1(IMAX-1,j)         ! down boundary
      ENDDO
      DO j = 1, IMAX
        T1(j,1)    = TL !T1(j,2)              ! left boundary
        T1(j,IMAX) = TR ! T1(j,IMAX-1)         ! right boudary
      ENDDO
      !
      time = time + dt  ! update time
      T    = T1         ! overwrite current solution
      !
      !
      ! 3.3) Eventually plot the results
      IF(ABS(time-tio).LT.1e-12) THEN
        WRITE(*,'(a,f15.7)') ' |  => plotting data output at time ', time
        CALL DataOutput(n,TestName,IMAX,x,y,T)
        tio = tio + dtio
      ENDIF
    ENDDO !n
    !
    !---------------------------------------- 
    !---------------------------------------- 
    !
    ! Empty memory
    !
    DEALLOCATE( x, T, T1 )
    !
    WRITE(*,'(a)') ' | '
    WRITE(*,'(a)') ' |         Finalization was successful. Bye :-)           | '
    WRITE(*,'(a)') ' | ====================================================== | '
    !
END PROGRAM Heat2D

SUBROUTINE DataOutput(timestep,TestName,IMAX,x,y,T)
    !------------------------------------------------------------!
    IMPLICIT NONE
    !------------------------------------------------------------!
    INTEGER,            INTENT(IN) :: timestep, IMAX
    CHARACTER(LEN=200), INTENT(IN) :: TestName
    REAL,               INTENT(IN) :: x(IMAX), y(IMAX), T(IMAX,IMAX)
    !
    INTEGER                        :: i, j, DataUnit
    CHARACTER(LEN=10)              :: citer
    CHARACTER(LEN=200)             :: IOFileName
    !------------------------------------------------------------!
    !
    WRITE(citer,'(i0.10)') timestep                        ! convert iteration number to string
    IOFileName = TRIM(TestName)//'-'//TRIM(citer)//'.dat' ! name of output file
    DataUnit   = 100                                      ! unit for output file
    !
    OPEN(UNIT=DataUnit, FILE=TRIM(IOFilename), STATUS='UNKNOWN', ACTION='WRITE')
    !
    ! Header
    !WRITE(DataUnit,*) IMAX
    ! Coordinates
    DO i = 1, IMAX                                      ! Order is row-column because it is
      DO j = 1, IMAX                                    ! used in python to plot array
        WRITE(DataUnit,'(f,f,f)') x(i),y(j),T(i,j)
      ENDDO  
    ENDDO
    ! Temperature
    !DO j = 1, IMAX
    !  DO i = 1, IMAX
    !    WRITE(DataUnit,*) T(i,j)
    !  ENDDO
    !ENDDO
    !
    CLOSE(DataUnit)
    !
END SUBROUTINE DataOutput
