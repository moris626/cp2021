PROGRAM Heat2D
    !------------------------------------------------------------!
    IMPLICIT NONE
    INCLUDE 'mpif.h'
    !------------------------------------------------------------!
    INTEGER             :: i, j, n         ! space and time index
    !
    CHARACTER(10)       :: cellchar
    INTEGER             :: IMAX            ! total number of cells 
    REAL                :: xL, xR          ! left and right domain definition
    REAL                :: yU, yD          ! up and down domain definition
    REAL                :: xD              ! location of discontinuity
    REAL                :: dx, dx2         ! mesh size and square of mesh size
    REAL, ALLOCATABLE   :: x(:), y(:)      ! vertex coords
    !
    REAL                :: CFL             ! Courant-Friedrichs-Lewy number for stability condition (CFL<1) 
    REAL                :: time            ! current time
    REAL                :: dt              ! time step
    REAL                :: tend            ! final time
    INTEGER, PARAMETER  :: NMAX = 1e6      ! maximum number of time steps 
    REAL, ALLOCATABLE   :: T(:,:), T1(:,:) ! temperature 
    REAL, ALLOCATABLE   :: local(:,:)
    !
    CHARACTER(LEN=200)  :: TestName        ! name of test problem
    REAL                :: tio             ! output time and time step 
    REAL                :: dtio            ! output time step
    !
    REAL                :: d               ! stability parameter  
    REAL                :: kappa           ! heat conduction coefficient
    REAL                :: TL, TR          ! boundary conditions
    !------------------------------------------------------------!
    !
    ! MPI added vars
    INTEGER              :: iErr
    INTEGER              :: nCPU, myrank
    INTEGER, ALLOCATABLE :: counts(:), displs(:)
    INTEGER              :: xlocalsize, realsize
    INTEGER              :: p
    INTEGER, DIMENSION(2):: sizes, subsizes, starts
    INTEGER, PARAMETER   :: root = 0
    INTEGER              :: newtype, resizedtype
    INTEGER(KIND=MPI_ADDRESS_KIND) :: extent, begin
    INTEGER              :: XMAX, YMAX
    INTEGER              :: status
    REAL, ALLOCATABLE    :: bufferR(:), bufferL(:)
    !------------------------------------------------------------!
    !First, make sure the right number of inputs have been provided
    IF(COMMAND_ARGUMENT_COUNT().NE.1)THEN
      WRITE(*,*)'ERROR, TOTAL NUMBER OF CELLS REQUIRED'
      STOP
    ENDIF    
    CALL GET_COMMAND_ARGUMENT(1,cellchar)
    READ(cellchar,*)IMAX
    !
    ! MPI initialization
    !   
    call MPI_Init(iErr)
    call MPI_Comm_size(MPI_COMM_WORLD, nCPU, iErr)
    call MPI_Comm_rank(MPI_COMM_WORLD, myrank, iErr)
    !
    !------------------------------------------------------------!    
    !
    ! ALL PROCESSES INITIALIZATION
    !
    TestName = 'Heat2D_mpi'
    !
    xL     = 0
    xR     = 1
    yU     = 1
    yD     = 0 
    xD     = 0.5   ! location of the discontinuity
    !
    time   = 0.0
    tend   = 0.1    
    dtio   = 2e-2
    tio    = dtio
    CFL    = 0.9   ! for stability condition CFL<1
    d      = 0.45  ! for stability condition d<0.5
    !
    kappa  = 1     ! heat conduction coefficient 
    !
    ! Boundary conditions
    TL = 100
    TR = 50
    !
    !
    !------------------------------------------------------------!
    !
    ! Global computation domain vars
    !
    dx    = (xR - xL) / REAL(IMAX)  ! step is the same
    dx2   = dx**2                   ! for x and y coords   
    xlocalsize = IMAX/nCPU +2
    allocate( local(IMAX,xlocalsize) )
    allocate( T1(IMAX,xlocalsize) )
    allocate(counts(nCPU))
    allocate(displs(nCPU))
    allocate(bufferL(IMAX))
    allocate(bufferR(IMAX))
    !
    !------------------------------------------------------------!
    !
    ! 1) ROOT PROCESS  initialization
    !
    IF (myrank.EQ.0) THEN
      WRITE(*,'(a)') ' | ====================================================== | '
      WRITE(*,'(a)') ' |      Finite difference code for 2D heat equation       | '
      WRITE(*,'(a)') ' | ====================================================== | '
      WRITE(*,'(a)') ' | '
      WRITE(*,'(a,i,i)') ' |  COMM: ', myrank ,nCPU
      !
      ALLOCATE( T(IMAX,IMAX+2)  )
      ALLOCATE( x(IMAX+2)  )
      ALLOCATE( y(IMAX)  )
      !
      !----------------------------------------
      !----------------------------------------
      !
      ! 1.1) Computational domain
      !
      WRITE(*,'(a)') ' | Building computational domain... '
      !
      x(1)  = xL
      y(1)  = yD
      !
      DO i = 1, IMAX+2
        x(i) = xL + 0.5*dx + (i-2)*dx
      ENDDO
      DO i = 1, IMAX
        y(i) = yD + 0.5*dx + (i-1)*dx
      ENDDO
      !
      !----------------------------------------
      !----------------------------------------
      !
      ! 2.1) Initial condition
      !
      WRITE(*,'(a)') ' | Assigning initial condition... '
      !
      DO j = 1, IMAX+2              
        DO i = 1, IMAX             
          IF(x(j).LT.xD) THEN
            T(i,j) = TL
          ELSE
            T(i,j) = TR
          ENDIF
        ENDDO
      ENDDO
    ENDIF   
    !
    !------------------------------------------------------------!
    !
    ! 2) MPI_Type definition
    !
    XMAX = IMAX/nCPU
    YMAX = IMAX
    WRITE(*,'(a)') ' | Define MPI Type... '   
    sizes    = [IMAX,IMAX+2]          ! size of global array
    subsizes = [YMAX,XMAX+2]          ! size of sub-region 
    starts   = [0,0]                  ! which begins at offset [0,0] 
    !
    CALL MPI_Type_create_subarray(2, sizes, subsizes, starts, MPI_ORDER_FORTRAN, MPI_REAL, newtype, iErr)
    CALL MPI_Type_size(MPI_REAL, realsize, iErr)
    begin = 0 
    extent = (YMAX)*realsize          !  set the extent as a columns size.  Scatter shift unit = 1 column   
    CALL MPI_Type_create_resized(newtype, begin, extent, resizedtype, iErr)
    CALL MPI_Type_commit(resizedtype, iErr)
    !
    !------------------------------------------------------------!
    !
    ! 3) Scatterv
    !
    WRITE(*,'(a)') ' | Prepare Scatter... '
    counts = 1                         !  send one of these new types to everyone: size = YMAX*XMAX+2
    DO i=1 ,nCPU 
      displs(i)= XMAX  * (i-1)  
    ENDDO 
    CALL MPI_Barrier(MPI_COMM_WORLD, iErr)
    CALL MPI_Scatterv(T, counts, displs,     & ! myrank proc gets counts(i) types from displs(i)
            resizedtype,                     &
            local, (2+XMAX)*YMAX , MPI_REAL, & ! receiving (2+XMAX)*YMAX  real 
            root, MPI_COMM_WORLD, iErr)        !... from (root, MPI_COMM_WORLD)
    !
    do p=1, nCPU                               ! check Scatter blocks
        if (myrank == p-1) then
            print *, 'Rank ', myrank, ' received: ', 'SHAPE', SHAPE(local)
            print *, local(:,:)
        endif
        call MPI_Barrier(MPI_COMM_WORLD, iErr)
    enddo
    !
    !------------------------------------------------------------!
    !
    ! 4) Core Processing
    !
    WRITE(*,'(a)') ' | '
    WRITE(*,'(a)') ' | Explicit finite difference solver. '
    WRITE(*,'(a)') ' | '
    !
    DO n = 1, NMAX
      !
      IF(MOD(n,100).EQ.0) THEN
      !  WRITE(*,'(a,i10)') '  Current timestep = ', n 
      ENDIF
      !
      ! 4.1) Compute the time step
      !
      IF(time.GE.tend) THEN
        EXIT    
      ENDIF
      !
      dt = d * dx2 / kappa
      IF((time+dt).GT.tend) THEN
        dt  = tend - time   
        tio = tend
      ENDIF    
      IF((time+dt).GT.tio) THEN
        dt = tio - time
      ENDIF
      !
      ! 4.2) Numerical scheme: FTCS
      !
      CALL OMP_SET_NUM_THREADS(4)

      !$OMP PARALLEL 
      !$OMP DO
      DO j = 2, XMAX+1    !single process -->(IMAX-1)
        DO i = 2, YMAX-1  !single process -->(IMAX-1)
          T1(i,j)= local(i,j)  + kappa*dt/(dx2) * ( local(i-1,j) - local(i,j) - local(i,j) + local(i+1,j) ) + kappa*dt/(dx2) *  (local(i,j-1) - local(i,j) - local(i,j) + local(i,j+1)) 
        ENDDO
      ENDDO
      !$OMP END DO
      !
      ! 4.3) Boundary Copy
      !
      !$OMP DO
      DO i = 2, XMAX+1                     ! Up and Down boudary
        T1(1,i)    = T1(2,i)
        T1(YMAX,i) = T1(YMAX-1,i)
      ENDDO
      !$OMP END DO
      if ((myrank.EQ.0).OR.(myrank.EQ.(nCPU-1))) THEN 
        !$OMP DO
        DO i = 1, YMAX                       ! Left and Right boundary (it will be replaced by ghost if process isn't in [1,nCPU])
          T1(i,1)    =   TL !T1(i,2)         !T1(i,2)         ! TODO To check if it is correct
          T1(i,2+XMAX) = TR !T1(i,1+XMAX)    !T1(i,1+XMAX)    ! TODO To check if it is correct
        ENDDO
        !$OMP END DO
      ENDIF
      !$OMP END PARALLEL
      !
      ! 4.4) Halo cells management
      !
      IF(MOD(myrank,2).EQ.0) THEN
        !
        ! 4.4.1) Even process send to right, then receive
        ! 
        IF ((myrank+1).NE.(nCPU)) THEN
          bufferR = T1(:,XMAX+1) 
          ! SEND TO RIGHT
          CALL MPI_SEND(bufferR, IMAX, MPI_REAL, (myrank+1), 11, MPI_COMM_WORLD,  iErr )
          ! RECEIVE FROM RIGHT
          CALL MPI_RECV(bufferL, IMAX, MPI_REAL, (myrank+1), 14, MPI_COMM_WORLD, status, iErr )
          T1(:,XMAX+2) = bufferL
        ENDIF
        !
        ! 4.4.2) Even process send to left, then receive 
        !
        IF ((myrank-1).NE.-1) THEN
          bufferL = T1(:,2)
          ! SEND TO LEFT
          CALL MPI_SEND(bufferL, IMAX, MPI_REAL, (myrank-1), 13, MPI_COMM_WORLD,  iErr )
          ! RECEIVE FROM LEFT
          CALL MPI_RECV(bufferR, IMAX, MPI_REAL, (myrank-1), 12, MPI_COMM_WORLD, status, iErr )
          T1(:,1)=bufferR
        ENDIF
      ELSE
        !
        ! 4.4.3) Od process receive from  left, then send
        !
        IF ((myrank-1).NE.-1) THEN
          ! RECEIVE FROM LEFT 
          CALL MPI_RECV(bufferR, IMAX, MPI_REAL, (myrank-1), 11, MPI_COMM_WORLD, status, iErr )
          T1(:,1)=bufferR
          bufferL = T1(:,2)
          ! SEND TO LEFT
          CALL MPI_SEND(bufferL, IMAX, MPI_REAL, (myrank-1), 14, MPI_COMM_WORLD,  iErr )         
        ENDIF
        !
        ! 4.4.4) Odd process receive from  right, then send
        !
        IF ((myrank+1).NE.(nCPU)) THEN
          ! RECEIVE FROM RIGHT 
          CALL MPI_RECV(bufferL, IMAX, MPI_REAL, (myrank+1), 13, MPI_COMM_WORLD, status, iErr)
          T1(:,XMAX+2)=bufferL
          bufferR = T1(:,XMAX+1)
          ! SEND TO RIGHT
          CALL MPI_SEND(bufferR, IMAX, MPI_REAL, (myrank+1), 12, MPI_COMM_WORLD,  iErr )
        ENDIF 
      ENDIF
      !
      time = time + dt          ! update time
      local    = T1             ! overwrite current solution
      !
      ! 4.5) Eventually plot the results
      IF(ABS(time-tio).LT.1e-12) THEN
        ! 4.5.1) Gatherv
        !
        call MPI_Gatherv( local, (2+XMAX)*YMAX , MPI_REAL, &        ! Sending (2+XMAX)*YMAX 
                          T, counts, displs, resizedtype,  &
                          root, MPI_COMM_WORLD, ierr)
        ! 4.5.2) Plot
        IF (myrank.EQ.0) THEN
          WRITE(*,'(a,f15.7)') ' |  => plotting data output at time ', time
          CALL DataOutput(n,TestName,IMAX,x,y,T)
        ENDIF
        tio = tio + dtio
      ENDIF  
      !

    ENDDO !n
    !
    !------------------------------------------------------------!
    !
    ! 5) Gatherv
    !
    !call MPI_Gatherv( local, (2+XMAX)*YMAX , MPI_REAL, &        ! Sending (2+XMAX)*YMAX 
    !                  T, counts, displs, resizedtype,  &
    !                  root, MPI_COMM_WORLD, ierr)
    !DEALLOCATE (local, T1)
    !
    !------------------------------------------------------------!
    !
    ! 6) Finalisation
    !
    IF (myrank.EQ.0) THEN
      !
      !---------------------------------------- 
      !---------------------------------------- 
      !
      ! Empty memory
      !
      DEALLOCATE( x,y, T )
      !
      WRITE(*,'(a)') ' | '
      WRITE(*,'(a)') ' |         Finalization was successful. Bye :-)           | '
      WRITE(*,'(a)') ' | ====================================================== | '
      !
    ENDIF
    call MPI_Barrier(MPI_COMM_WORLD, iErr)
END PROGRAM Heat2D

SUBROUTINE DataOutput(timestep,TestName,IMAX,x,y,T)
    !------------------------------------------------------------!
    IMPLICIT NONE
    !------------------------------------------------------------!
    INTEGER,            INTENT(IN) :: timestep, IMAX
    CHARACTER(LEN=200), INTENT(IN) :: TestName
    REAL,               INTENT(IN) :: x(IMAX+2), y(IMAX), T(IMAX,IMAX+2)
    !
    INTEGER                        :: i, j, DataUnit
    CHARACTER(LEN=10)              :: citer
    CHARACTER(LEN=200)             :: IOFileName
    !------------------------------------------------------------!
    !
    WRITE(citer,'(i0.10)') timestep                       ! convert iteration number to string
    IOFileName = TRIM(TestName)//'-'//TRIM(citer)//'.dat' ! name of output file
    DataUnit   = 100                                      ! unit for output file
    !
    OPEN(UNIT=DataUnit, FILE=TRIM(IOFilename), STATUS='UNKNOWN', ACTION='WRITE')
    !
    ! Header
    !WRITE(DataUnit,*) IMAX
    ! Coordinates
    DO i = 1, IMAX                                        ! Order is row-column because it is 
      DO j = 1, IMAX                                      ! used in python to plot array
        WRITE(DataUnit,'(f,f,f)') x(j+1),y(i),T(i,j+1)
      ENDDO  
    ENDDO
    !
    CLOSE(DataUnit)
    !
END SUBROUTINE DataOutput
