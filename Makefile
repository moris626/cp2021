# *********************************************************************
#  makefile for 1D heat code
# *********************************************************************
#
.SUFFIXES : .o .f90 
.KEEP_STATE:
#
EXE  = heat
# *********************************************************************
F90  = mpiifort 

#------------------------------------------------------------------------------
# INCLUDE PATH
#------------------------------------------------------------------------------

BINDIR   = ./bin 

#------------------------------------------------------------------------------
# LIBRARIES
#------------------------------------------------------------------------------

#LFLAGS = -cxxlib 

# COMPILATION OPTIMIZATION FLAGS for ifort
#F90FLAGS = -c -g -O0 -r8 -debug -traceback -check bounds -check pointers -check uninit -align -assume byterecl -fpp -heap-arrays 2500
F90FLAGS = -c -O2 -r8 -align -assume byterecl -fpp

#FCFLAGS=-DPARALLEL


#
.SUFFIXES: .f90 

.f90.o:
	@echo "   "
	@echo " Compiling Source File: " $<
	@echo " ---------------------- ===================="
	$(F90) $(F90FLAGS) $(FCFLAGS) $(INCLUDES) $<

# Compiled object files
 
F90_OBJ = Heat1D_MPI.o 

OBJS       = $(F90_OBJ) 
FOR_FILES  = $(OBJS:.o=.f90)

Executable: $(EXE)

$(EXE): Makefile $(OBJS)
	@echo "   "
	@echo " Now Linking..."
	@echo " --------------"
	$(F90) $(OBJS) $(LFLAGS)  -o $(EXE)

	@mv $@ $(BINDIR)
	@echo "==============================================================="
	@echo '=    HEAT1D_MPI installed in ' $(BINDIR)
	@echo "==============================================================="

#
clean:
	rm -f core* *.M *.o $(EXE) *.kmo *.mod 

#
flush:
	rm -f core* *.M *.o $(EXE) *.kmo *.mod 
	rm -f *.dat 
	rm -rf ii_files
	rm -f *.pc *.pcl
#
dat:	
	rm -f *.dat *.last *.log *.plt *.txt
#
cor: 
	rm -f core*
#
tilde:
	rm -f *~
#
no_exe:
	rm -f $(EXE)
#
mv:
	mv *.o *.mod ./obj
