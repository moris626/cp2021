# README #

Progetto calcolo parallelo

### Tips ###

Dopo aver installato ifort bisogna fare il source

```bash
. /opt/intel/oneapi/setvars.sh
```


altrimenti non va niente 

### Execute ###

```bash

cd Heat2D

.run.sh NCPU DOMAIN_SIZE
```

### Final presentation ###

Heat2D/PP.pdf

